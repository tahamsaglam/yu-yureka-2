#!/usr/bin/env python
import os
import sys
import time
import xlrd
import xlwt
import commands


def generate():
    cmd  = "repo forall -pc git log --since='1 day ago' --pretty=format:'%s' > .repo/gen_logs_tmpfile"
    os.system(cmd)
    f=open(".repo/gen_logs_tmpfile")
    lines=f.readlines()
    i=1
    wbk = xlwt.Workbook(encoding='utf-8')
    sheet = wbk.add_sheet('she9')
    sheet.write(0,0,'Modify Reason')
    sheet.write(0,1,'Product')
    sheet.write(0,2,'Modify Methods')
    sheet.write(0,3,'Test Requirements')
    sheet.write(0,4,'Ower')
    sheet.write(0,5,'Reviewer')
    for one in lines:
        if not one.startswith("Modify Reason") :
            pass
        else:
            try:
                info1 = one.split("Modify Reason")[1].split("Product")[0]
                if info1.startswith(":"):
                    info1 = info1[1:]
                else:
                    info1 = info1[3:]
            except Exception:
                info1 = ""
            if "Modify Methodsi" not in one:
                try:
                    info2 = one.split("Modify Methods")[1].split("Test Requirements")[0]
                    if info2.startswith(":"):
                        info2 = info2[1:]
                    else:
                        info2 = info2[3:]
                except Exception:
                    info2 = ""
            try:
                info3 = one.split("Test Requirements")[1].split("Ower")[0]
                if info3.startswith(":"):
                    info3 = info3[1:]
                else:
                    info3 = info3[3:]
            except Exception:
                info3 = ""
            if info3 == "":
                try:
                    info3 = one.split("Test Requirements")[1].split("Owner")[0]
                    if info3.startswith(":"):
                        info3 = info3[1:]
                    else:
                        info3 = info3[3:]
                except Exception:
                    info3 = ""
            try:
                info4 = one.split("Ower")[1].split("Reviewer")[0]
                if info4.startswith(":"):
                    info4 = info4[1:]
                else:
                    info4 = info4[3:]
            except Exception:
                info4 = ""
            try:
                info5 = one.split("Reviewer")[1].split("Comments")[0]
                if info5.startswith(":"):
                    info5 = info5[1:]
                else:
                    info5 = info5[3:]
            except Exception:
                info5 = ""
            try:
                info6 = one.split("Product")[1].split("Reference Git")[0]
                if info6.startswith(":"):
                    info6 = info6[1:]
                else:
                    info6 = info6[3:]
            except Exception:
                info6 = ""
            sheet.write(i,0,info1)
            sheet.write(i,1,info6)
            sheet.write(i,2,info2)
            sheet.write(i,3,info3)
            sheet.write(i,4,info4)
            sheet.write(i,5,info5)
            i = i + 1
    wbk.save('OUT.xls')
    print "\nGenerate Successed !"
    print "="*80
    
    

if __name__ == '__main__':
    generate()



