#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import sys
import time
import commands
import getopt

def mymkd(path):
    os.system("mkdir -p %s"%path)
    os.system("touch %s/.gitignore"%path)

newproject = sys.argv[1]

os.chdir("android")
lists = commands.getoutput("ls -1 --ignore Android.mk wingcust").split("\n")
print "Current projects:"
for one in lists:
    print "  %s"%one
basep = raw_input("Choose the father project:")
if not basep in lists:
    print "Error: Illegal father project!"
    sys.exit(-1)
mymkd("wingcust/%s/%s"%(basep,newproject))
mymkd("wingcust/%s/%s/device/overlay"%(basep,newproject))
mymkd("wingcust/%s/%s/google"%(basep,newproject))
mymkd("wingcust/%s/%s/manifest"%(basep,newproject))
mymkd("wingcust/%s/%s/product"%(basep,newproject))
mymkd("wingcust/%s/%s/wt_overlay"%(basep,newproject))
mymkd("wingcust/%s/%s/checklists"%(basep,newproject))
os.system("cp tools/wingtechtools/checklists/* wingcust/%s/%s/checklists"%(basep,newproject))
print "==================================\nSuccessed!"
